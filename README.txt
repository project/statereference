WHAT IT DOES:
-------------
This module adds a new CCK field type for referencing workflow states in CCK content types.

TO INSTALL:
-----------
Drop the statereference folder into the 'modules'
directory of your drupal installation.

REQUIRES:
---------
CCK
http://drupal.org/project/cck

Worflow module
http://drupal.org/project/workflow

BUGS & ISSUES:
--------------
http://drupal.org/project/issues/statereference

SPONSOR:
--------
Classic Graphics <http://www.cgraphics.com/>